<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\backend\ProfileController;
use App\Http\Controllers\Backend\Setup\AssignSubjectController;
use App\Http\Controllers\Backend\Setup\DesignationController;
use App\Http\Controllers\Backend\Setup\ExamTypeController;
use App\Http\Controllers\Backend\Setup\FeeAmountController;
use App\Http\Controllers\Backend\Setup\FeeCategoryController;
use App\Http\Controllers\Backend\Setup\SchoolSubjectController;
use App\Http\Controllers\Backend\Setup\StudentClassController;
use App\Http\Controllers\Backend\Setup\StudentGroupController;
use App\Http\Controllers\Backend\Setup\StudentShiftController;
use App\Http\Controllers\Backend\Setup\StudentYearController;
use App\Http\Controllers\Backend\Students\StudentRegistrationController;
use App\Http\Controllers\Backend\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    Route::get('/dashboard', function () {
        return view('admin.index');
    })->name('dashboard');
});

Route::get('/admin/logout', [AdminController::class,'Logout'])->prefix('users')->name('admin.logout');


//Users Routes
Route::prefix('users')->group(function(){
    Route::get('/view', [UserController::class,'UserView'])->name('user.view');
    Route::get('/add', [UserController::class,'UserAdd'])->name('user.add');
    Route::post('/store', [UserController::class,'UserStore'])->name('user.store');
    Route::get('/edit/{id}', [UserController::class,'UserEdit'])->name('user.edit');
    Route::post('/update/{id}',[UserController::class, 'UserUpdate'])->name('user.update');
    Route::get('/delete/{id}', [UserController::class,'UserDestroy'])->name('user.delete');
});


//User profile  and password routes

Route::prefix('profile')->group(function(){
    Route::get('/view',[ProfileController::class,'ProfileView'])->name('profile.view');
    Route::get('/edit',[ProfileController::class,'ProfileEdit'])->name('profile.edit');
    Route::post('/update',[ProfileController::class, 'ProfileUpdate'])->name('profile.update');
    Route::get('/password/view',[ProfileController::class,'PasswordView'])->name('password.view');
    Route::post('/password/update',[ProfileController::class,'PasswordUpdate'])->name('passwordd.update');
});



Route::prefix('setups')->group(function(){
    //Student class routes
    Route::get('/student/class/view',[StudentClassController::class,'ViewStudent'])->name('student.class.view');
    Route::get('/student/class/add',[StudentClassController::class,'StudentClassAdd'])->name('student.class.add');
    Route::post('/student/class/store', [StudentClassController::class,'StudentClassStore'])->name('student.class.store');
    Route::get('/student/class/edit/{id}', [StudentClassController::class,'StudentClassEdit'])->name('student.class.edit');
    Route::post('/student/class/update/{id}',[StudentClassController::class, 'StudentClassUpdate'])->name('student.class.update');
    Route::get('/student/class/delete/{id}', [StudentClassController::class,'StudentClassDestroy'])->name('student.class.delete');

    //Student year routes
    Route::get('/student/year/view',[StudentYearController::class,'ViewStudentYear'])->name('student.year.view');
    Route::get('/student/year/add',[StudentYearController::class,'StudentYearAdd'])->name('student.year.add');
    Route::post('/student/year/store', [StudentYearController::class,'StudentYearStore'])->name('student.year.store');
    Route::get('/student/year/edit/{id}', [StudentYearController::class,'StudentYearEdit'])->name('student.year.edit');
    Route::post('/student/year/update/{id}',[StudentYearController::class, 'StudentYearUpdate'])->name('student.year.update');
    Route::get('/student/year/delete/{id}', [StudentYearController::class,'StudentYearDestroy'])->name('student.year.delete');

    //Student group routes
    Route::get('/student/group/view',[StudentGroupController::class,'ViewStudentGroup'])->name('student.group.view');
    Route::get('/student/group/add',[StudentGroupController::class,'StudentGroupAdd'])->name('student.group.add');
    Route::post('/student/group/store', [StudentGroupController::class,'StudentGroupStore'])->name('student.group.store');
    Route::get('/student/group/edit/{id}', [StudentGroupController::class,'StudentGroupEdit'])->name('student.group.edit');
    Route::post('/student/group/update/{id}',[StudentGroupController::class, 'StudentGroupUpdate'])->name('student.group.update');
    Route::get('/student/group/delete/{id}', [StudentGroupController::class,'StudentGroupDestroy'])->name('student.group.delete');


    //Student shift routes
    Route::get('/student/shift/view',[StudentShiftController::class,'ViewStudentShift'])->name('student.shift.view');
    Route::get('/student/shift/add',[StudentShiftController::class,'StudentShiftAdd'])->name('student.shift.add');
    Route::post('/student/shift/store', [StudentShiftController::class,'StudentShiftStore'])->name('student.shift.store');
    Route::get('/student/shift/edit/{id}', [StudentShiftController::class,'StudentShiftEdit'])->name('student.shift.edit');
    Route::post('/student/shift/update/{id}',[StudentShiftController::class, 'StudentShiftUpdate'])->name('student.shift.update');
    Route::get('/student/shift/delete/{id}', [StudentShiftController::class,'StudentShiftDestroy'])->name('student.shift.delete');

    //Fee category routes
    Route::get('/fee/categories/view',[FeeCategoryController::class,'ViewFeeCategory'])->name('fee.category.view');
    Route::get('/fee/categories/add',[FeeCategoryController::class,'FeeCategoryAdd'])->name('fee.category.add');
    Route::post('/fee/categories/store', [FeeCategoryController::class,'FeeCategoryStore'])->name('fee.category.store');
    Route::get('/fee/categories/edit/{id}', [FeeCategoryController::class,'FeeCategoryEdit'])->name('fee.category.edit');
    Route::post('/fee/categories/update/{id}',[FeeCategoryController::class, 'FeeCategoryUpdate'])->name('fee.category.update');
    Route::get('/fee/categories/delete/{id}', [FeeCategoryController::class,'FeeCategoryDestroy'])->name('fee.category.delete');

    //Fee category amount routes
    Route::get('/fee/amount/view',[FeeAmountController::class,'ViewFeeAmount'])->name('fee.amount.view');
    Route::get('/fee/amount/add',[FeeAmountController::class,'FeeAmountAdd'])->name('fee.amount.add');
    Route::post('/fee/amount/store', [FeeAmountController::class,'FeeAmountStore'])->name('fee.amount.store');
    Route::get('/fee/amount/edit/{id}', [FeeAmountController::class,'FeeAmountEdit'])->name('fee.amount.edit');
    Route::post('/fee/amount/update/{id}',[FeeAmountController::class, 'FeeAmountUpdate'])->name('fee.amount.update');
    Route::get('/fee/amount/details/{id}', [FeeAmountController::class,'FeeAmountDetails'])->name('fee.amount.details');
    Route::get('/fee/amount/delete/{id}', [FeeAmountController::class,'FeeAmountDestroy'])->name('fee.amount.delete');


    //Exam Type routes
    Route::get('/exam/type/view',[ExamTypeController::class,'ViewExamType'])->name('exam.type.view');
    Route::get('/exam/type/add',[ExamTypeController::class,'ExamTypeAdd'])->name('exam.type.add');
    Route::post('/exam/type/store', [ExamTypeController::class,'ExamTypeStore'])->name('exam.type.store');
    Route::get('/exam/type/edit/{id}', [ExamTypeController::class,'ExamTypeEdit'])->name('exam.type.edit');
    Route::post('/exam/type/update/{id}',[ExamTypeController::class, 'ExamTypeUpdate'])->name('exam.type.update');
    Route::get('/exam/type/delete/{id}', [ExamTypeController::class,'ExamTypeDestroy'])->name('exam.type.delete');


     //School Subjects routes
     Route::get('/school/subject/view',[SchoolSubjectController::class,'ViewSchoolSubject'])->name('school.subject.view');
     Route::get('/school/subject/add',[SchoolSubjectController::class,'SchoolSubjectAdd'])->name('school.subject.add');
     Route::post('/school/subject/store', [SchoolSubjectController::class,'SchoolSubjectStore'])->name('school.subject.store');
     Route::get('/school/subject/edit/{id}', [SchoolSubjectController::class,'SchoolSubjectEdit'])->name('school.subject.edit');
     Route::post('/school/subject/update/{id}',[SchoolSubjectController::class, 'SchoolSubjectUpdate'])->name('school.subject.update');
     Route::get('/school/subject/delete/{id}', [SchoolSubjectController::class,'SchoolSubjectDestroy'])->name('school.subject.delete');


     //Assign subjects to class routes
    Route::get('/assign/subject/view',[AssignSubjectController::class,'ViewAssignSubject'])->name('assign.subject.view');
    Route::get('/assign/subject/add',[AssignSubjectController::class,'AssignSubjectAdd'])->name('assign.subject.add');
    Route::post('/assign/subject/store', [AssignSubjectController::class,'AssignSubjectStore'])->name('assign.subject.store');
    Route::get('/assign/subject/edit/{id}', [AssignSubjectController::class,'AssignSubjectEdit'])->name('assign.subject.edit');
    Route::post('/assign/subject/update/{id}',[AssignSubjectController::class, 'AssignSubjectUpdate'])->name('assign.subject.update');
    Route::get('/assign/subject/details/{id}', [AssignSubjectController::class,'AssignSubjectDetails'])->name('assign.subject.details');
    Route::get('/assign/subject/delete/{id}', [AssignSubjectController::class,'AssignSubjectDestroy'])->name('assign.subject.delete');


    //Designation routes

    Route::get('/designation/view',[DesignationController::class,'ViewDesignation'])->name('designation.view');
    Route::get('/designation/add',[DesignationController::class,'DesignationAdd'])->name('designation.add');
    Route::post('/designation/store', [DesignationController::class,'DesignationStore'])->name('designation.store');
    Route::get('/designation/edit/{id}', [DesignationController::class,'DesignationEdit'])->name('designation.edit');
    Route::post('/designation/update/{id}',[DesignationController::class, 'DesignationUpdate'])->name('designation.update');
    Route::get('/designation/delete/{id}', [DesignationController::class,'DesignationDestroy'])->name('designation.delete');

 


   
});



Route::prefix('students')->group(function(){
    Route::get('/registration/view',[StudentRegistrationController::class,'ViewStudentRegistration'])->name('registration.view');
});
