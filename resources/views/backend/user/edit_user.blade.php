@extends('admin.admin_master')
@section('admin')

<div class="content-wrapper">
<div class="container-full">
<!-- Main content -->
<section class="content">

<!-- Basic Forms -->
<div class="box">
    <div class="box-header with-border">

        <h4 class="box-title"> <i class="fa fa-pencil-square-o" aria-hidden="true"> Update User</i></h4>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="row">
            <div class="col">
                <form  method="post" action="{{route('user.update',$userData->id)}}">
                    @csrf
                    <div class="row">
                        <div class="col-12">

                            <div class="row">
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <h5>Role <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <select name="usertype" id="usertype" required class="form-control">
                                                <option value="" selected="" disabled="">Select User Role</option>
                                                <option value="Administrator" {{ ($userData->usertype == "Administrator" ? "selected":"")}}>Administrator</option>
                                                <option value="User" {{ ($userData->usertype == "User" ? "selected":"")}}>User</option>


                                            </select>
                                        </div>
                                    </div>


                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <h5>Full Name <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="name" id="name" class="form-control" value="{{$userData->name}}" required>
                                        </div>
                                    </div>

                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <h5>Email <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="email" name="email" id="email" class="form-control" value="{{$userData->email}}" required>
                                        </div>
                                    </div>


                                </div>

                                <div class="col-md-6">
                                <div class="form-group">
                                <h5>Status <span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <select name="status" id="status" required class="form-control">
                                        <option value="" selected="" disabled="">Select User dstatus</option>
                                        <option value="1" {{ ($userData->status == 1 ? "selected":"")}}>Active</option>
                                        <option value="0" {{ ($userData->status == 0  ? "selected":"")}}>Not Active</option>


                                    </select>
                                </div>
                            </div>

                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="text-xs-right">
                    <input type="submit" class="btn btn-rounded btn-info mb-5" value="Update" />


                        {{--<button class="btn btn-rounded btn-info mb-5" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update</button>
                        --}}     <a class="btn btn-rounded btn-danger mb-5" href="{{route('user.view')}}"><i class="fa fa-fw fa-lg fa-arrow-left"></i>Go Back</a>
            
                    </div>
                </form>

            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->

</section>

<!-- /.content -->

</div>
</div>

@endsection