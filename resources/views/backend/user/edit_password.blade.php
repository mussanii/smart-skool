@extends('admin.admin_master')
@section('admin')

<div class="content-wrapper">
<div class="container-full">
<!-- Main content -->
<section class="content">

<!-- Basic Forms -->
<div class="box">
    <div class="box-header with-border">

        <h4 class="box-title"> <i class="fa fa-pencil-square-o" aria-hidden="true"> Change Password</i></h4>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="row">
            <div class="col">
                <form  method="post" action="{{route('passwordd.update')}}">
                    @csrf
                    <div class="row">
                        <div class="col-12">

                           


                            

                    <div class="form-group">
                        <h5>Current password <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="password" name="current_password" id="current_password" class="form-control" value="" required>
                            @error('current_password')
                            <span class="text-danger">{{$message}}</span>
                                
                            @enderror
                        </div>
                    </div> 


                    <div class="form-group">
                        <h5>New password <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="password" name="password" id="password" class="form-control" value="" required>
                            @error('password')
                            <span class="text-danger">{{$message}}</span>
                                
                            @enderror
                        </div>
                    </div> 


                    <div class="form-group">
                        <h5>Confirm password <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" value="" required>
                            @error('password_confirmation')
                            <span class="text-danger">{{$message}}</span>
                                
                            @enderror
                        </div>
                    </div> 

                        </div>
                    </div>

                    <div class="text-xs-right">
                    <input type="submit" class="btn btn-rounded btn-info mb-5" value="Update" />

     <a class="btn btn-rounded btn-danger mb-5" href="{{route('user.view')}}"><i class="fa fa-fw fa-lg fa-arrow-left"></i>Go Back</a>
            
                    </div>
                </form>

            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->

</section>

<!-- /.content -->

</div>
</div>

@endsection