@extends('admin.admin_master')
@section('admin')

<div class="content-wrapper">
    <div class="container-full">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">

                <div class="box box-widget widget-user">
					<!-- Add the bg color to the header using any of the bg-* classes -->
					<div class="widget-user-header bg-black" >
					  <h3 class="widget-user-username">{{$user->name}}</h3>
                      <a href="{{ route('profile.edit')}}" style="float: right;" class="btn btn-rounded btn-success mb-5"><i class="fa fa-pencil" aria-hidden="true"></i>
                                Edit Profile</a>
					  <h6 class="widget-user-desc">{{$user->usertype}}</h6>
                      <h6 class="widget-user-desc">{{$user->email}}</h6>
            
                 
                     
					</div>
					<div class="widget-user-image">
					  <img class="rounded-circle" src=" {{(!empty($user->image)) ? url('uploads/user_image/'.$user->image) : url('uploads/user_image/no_image.jpg') }}" alt="User Avatar">
					</div>
					<div class="box-footer">
					  <div class="row">
						<div class="col-sm-4">
						  <div class="description-block">
							<h5 class="description-header">{{$user->mobile}}</h5>
							<span class="description-text">Mobile Number</span>
						  </div>
						  <!-- /.description-block -->
						</div>
						<!-- /.col -->
						<div class="col-sm-4 br-1 bl-1">
						  <div class="description-block">
							<h5 class="description-header">{{$user->address}}</h5>
							<span class="description-text">Address</span>
						  </div>
						  <!-- /.description-block -->
						</div>
						<!-- /.col -->
						<div class="col-sm-4">
						  <div class="description-block">
                            @if($user->status ==1)
							<h5 class="description-header"> <span class="badge badge-success">Active</span></h5>
                                        
                            @else
                            <h5 class="description-header"><span class="badge badge-danger">Not Active</span></h5>
                            @endif
							<span class="description-text">Status</span>
						  </div>
						  <!-- /.description-block -->
						</div>
						<!-- /.col -->
					  </div>
					  <!-- /.row -->
					</div>
				  </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->

    </div>
</div>

@endsection