@extends('admin.admin_master')
@section('admin')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>

<div class="content-wrapper">
<div class="container-full">
<!-- Main content -->
<section class="content">

<!-- Basic Forms -->
<div class="box">
    <div class="box-header with-border">

        <h4 class="box-title"> <i class="fa fa-pencil-square-o" aria-hidden="true"> Edit Profile</i></h4>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="row">
            <div class="col">
                <form  method="post" action="{{route('profile.update')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-12">

                            <div class="row">
                               

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <h5>Full Name <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="name" id="name" class="form-control" value="{{$userData->name}}" required>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-6">

                                    <div class="form-group">
                                        <h5>Email <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="email" name="email" id="email" class="form-control" value="{{$userData->email}}" required>
                                        </div>
                                    </div>


                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <h5>Mobile No <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="mobile" id="mobile" class="form-control" value="{{$userData->mobile}}" required>
                                        </div>
                                    </div>


                                </div>

                                <div class="col-md-6">
                                     <div class="form-group">
                                        <h5>Address <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" value="{{$userData->address}}" name="address" id="address" class="form-control" required>
                                        </div>
                                    </div> 

                                </div>
                            </div>


                           


                            <div class="row">

                            <div class="col-md-6">

                            <div class="form-group">
                                <h5>Gender<span class="text-danger">*</span></h5>
                                <div class="controls">
                                <select name="gender" id="gender" required class="form-control">
                                                <option value="" selected="" disabled="">Select Gender</option>
                                                <option value="M" {{ ($userData->gender == "M" ? "selected":"")}}>Male</option>
                                                <option value="F" {{ ($userData->gender == "F" ? "selected":"")}}>Female</option>
                                                <option value="U" {{ ($userData->gender == "U" ? "selected":"")}}>Undefined</option>

                                            </select>
                                </div>
                            </div>


                                </div>


                                <div class="col-md-6">

                            <div class="form-group">
                                <h5>Profile Image<span class="text-danger">*</span></h5>
                                <div class="controls">
                                <input type="file" name="image" id="image" class="form-control" >
                                </div>
                            </div>

                            <div class="form-group">
                              
                                <div class="controls">
                               <img  id="showImage" src="{{(!empty($user->image)) ? url('uploads/user_image/'.$user->image) : url('uploads/user_image/no_image.jpg') }}" style="width: 100px; height:100px; border-radius:50%" alt="selected image" >
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

                    <div class="text-xs-right">
                    <input type="submit" class="btn btn-rounded btn-info mb-5" value="Update" />


                 <a class="btn btn-rounded btn-danger mb-5" href="{{route('profile.view')}}"><i class="fa fa-fw fa-lg fa-arrow-left"></i>Go Back</a>
            
                    </div>
                </form>

            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->

</section>

<!-- /.content -->

</div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#image').change((e)=>{
            var reader = new FileReader();
            reader.onload = function(e){
                $('#showImage').attr('src',e.target.result);
            }
            reader.readAsDataURL(e.target.files['0']);

        });

    })

</script>

@endsection