@extends('admin.admin_master')
@section('admin')

<div class="content-wrapper">
    <div class="container-full">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">

                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title"> <i class="fa fa-users" aria-hidden="true"></i>
                                Assign Subject List</h3>
                            <a href="{{ route('assign.subject.add')}}" style="float: right;" class="btn btn-rounded btn-success mb-5"><i class="fa fa-user-plus" aria-hidden="true"></i>
                                Assign subject</a>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                           
                            <div class="table-responsive">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="5%">SRL#</th>
                                            
                                            <th>Class</th>
                                            <th>Actions</th>
                                           

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ( $allData as $key=> $subject )

                                        
                                            
                                     
                                        <tr>
                                            <td>{{$key + 1}}</td>
                                           
                                            <td>{{$subject->classes->name}}</td>
                                            
                                           
                                            <td>
                                            <div class="btn-group " role="group" aria-label="Second group">
                                            <a href="{{route('assign.subject.details',$subject->class_id)}}" title="View" class="btn btn-sm btn-default mx-10 "><i class="fa fa-eye"></i></a>
                                               <a href="{{route('assign.subject.edit',$subject->class_id)}}" title="Edit" class="btn btn-sm btn-primary "><i class="fa fa-edit"></i></a>
                                                <a href="{{route('assign.subject.delete',$subject->class_id)}}" title="Delete" id="delete" class="btn btn-sm btn-danger mx-10"><i class="fa fa-trash"></i></a>
                                            </div>
                                            </td>
                                        </tr>
                                        @endforeach

                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>SRL#</th>
                                           
                                            <th>Class</th>
                                            <th>Actions</th>
                                            
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->


                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->

    </div>
</div>

@endsection