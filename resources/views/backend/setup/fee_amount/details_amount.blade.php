@extends('admin.admin_master')
@section('admin')

<div class="content-wrapper">
    <div class="container-full">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">

                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title"> <i class="fa fa-users" aria-hidden="true"></i>
                                Fee Amount Details</h3>
                            <a href="{{ route('fee.amount.view')}}" style="float: right;" class="btn btn-rounded btn-success mb-5"><i class="fa fa-user-plus" aria-hidden="true"></i>
                                Fee Categories</a>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                        <h4>Fee Amount Details for:<strong>{{$detailsData[0]->fee_category->name}} </strong></h4>
                            <div class="table-responsive">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="5%">SRL#</th>
                                            
                                            <th>Class Name</th>
                                            <th>Amount</th>
                                            <th>Creation Date</th>
                                           
                                           

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ( $detailsData as $key=> $fee )

                                        
                                            
                                     
                                        <tr>
                                            <td>{{$key + 1}}</td>
                                           
                                            <td>{{$fee->classes->name}}</td>
                                            <td>{{$fee->amount}}</td>
                                            <td>{{$fee->created_at}}</td>

                                            
                                           
                                          
                                        </tr>
                                        @endforeach

                                    </tbody>
                                    <tfoot>
                                        <tr>
                                        <th width="5%">SRL#</th>
                                            
                                            <th>Class Name</th>
                                            <th>Amount</th>
                                            <th>Creation Date</th>
                                            
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->


                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->

    </div>
</div>

@endsection