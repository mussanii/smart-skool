@extends('admin.admin_master')
@section('admin')

<div class="content-wrapper">
    <div class="container-full">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">

                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title"> <i class="fa fa-users" aria-hidden="true"></i>
                                Exam Type List</h3>
                            <a href="{{ route('exam.type.add')}}" style="float: right;" class="btn btn-rounded btn-success mb-5"><i class="fa fa-user-plus" aria-hidden="true"></i>
                                Add Exam Type</a>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="5%">SRL#</th>
                                            
                                            <th>Name</th>
                                           <th>Created </th>
                                           <th>Updated </th>
                                            <th>Action</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ( $allData as $key=> $student )

                                        
                                            
                                     
                                        <tr>
                                            <td>{{$key + 1}}</td>
                                           
                                            <td>{{$student->name}}</td>
                                            
                                            <td>{{$student->created_at->diffForHumans()}}</td>
                                            <td>{{$student->updated_at->diffForHumans()}}</td>
                                            
                                            <td>
                                            <div class="btn-group " role="group" aria-label="Second group">
                                      
                                                <a href="{{route('exam.type.edit',$student->id)}}" title="Edit" class="btn btn-sm btn-primary "><i class="fa fa-edit"></i></a>
                                                <a href="{{route('exam.type.delete',$student->id)}}" title="Delete" id="delete" class="btn btn-sm btn-danger mx-10"><i class="fa fa-trash"></i></a>
                                            </div>
                                            </td>
                                        </tr>
                                        @endforeach

                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>SRL#</th>
                                           
                                            <th>Name</th>
                                       
                                            <th>Created</th>
                                            <th>Updated</th>
                                          
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->


                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->

    </div>
</div>

@endsection