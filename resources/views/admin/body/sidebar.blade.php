@php
  $prefix = Request::route()->getprefix();
  $route = Route::current()->getName();

 


@endphp

<aside class="main-sidebar">
  <!-- sidebar-->
  <section class="sidebar">

    <div class="user-profile">
      <div class="ulogo">
        <a href="index.html">
          <!-- logo for regular state and mobile devices -->
          <div class="d-flex align-items-center justify-content-center">
            <img src="{{asset('backend/images/techsys_logo.png')}}" height="100px" alt="">
            <!-- <h3><b>Techsys</b> School ERP</h3> -->
          </div>
        </a>
      </div>
    </div>

    <!-- sidebar menu-->
    <ul class="sidebar-menu" data-widget="tree">

      <li class="{{ ($route =='dashboard') ? 'active':'' }}">
        <a href="{{route('dashboard')}}">
          <i data-feather="pie-chart"></i>
          <span>Dashboard</span>
        </a>
      </li>

      <li class="treeview {{ ($prefix == 'users')? 'active':''}}">
        <a href="#">
          <i data-feather="users"></i>
          <span>Manage Users</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-right pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('user.view')}}"><i class="ti-more"></i>View Users</a></li>
          <li><a href="{{route('user.add')}}"><i class="ti-more"></i>Add User</a></li>
        </ul>
      </li>

      <li class="treeview {{ ($prefix == 'profile')? 'active':''}}">
        <a href="#">
          <i data-feather="user"></i> <span>Manage Profile</span>
          <span class="pull-right-container">
            <i class="fa fa-user pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('profile.view')}}"><i class="ti-more"></i>My Profile</a></li>
          <li><a href="{{route('password.view')}}"><i class="ti-more"></i>Change Password</a></li>
        
        </ul>
      </li>

      <li class="treeview {{ ($prefix == 'setups')? 'active':''}}">
        <a href="#">
          <i data-feather="settings"></i> <span>Setup Management</span>
          <span class="pull-right-container">
            <i class="fa fa-user pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('student.class.view')}}"><i class="ti-more"></i>Student Classes</a></li>
          
      

          <li><a href="{{route('student.year.view')}}"><i class="ti-more"></i>Student Years</a></li>
        
          <li><a href="{{route('student.group.view')}}"><i class="ti-more"></i>Student Groups</a></li>

          <li><a href="{{route('student.shift.view')}}"><i class="ti-more"></i>Student Shifts</a></li>
   
          <li><a href="{{route('fee.category.view')}}"><i class="ti-more"></i>Fee Categories</a></li>

          <li><a href="{{route('fee.amount.view')}}"><i class="ti-more"></i>Fee Category Amount</a></li>

          <li><a href="{{route('exam.type.view')}}"><i class="ti-more"></i>Exam Type</a></li>

          <li><a href="{{route('school.subject.view')}}"><i class="ti-more"></i>Subjects</a></li>

          <li><a href="{{route('assign.subject.view')}}"><i class="ti-more"></i>Assign Subjects</a></li>

          <li><a href="{{route('designation.view')}}"><i class="ti-more"></i>Designation</a></li>
        </ul>
 
      </li>






      <li class="treeview {{ ($prefix == 'students')? 'active':''}}">
        <a href="#">
          <i data-feather="books"></i> <span>Student Management</span>
          <span class="pull-right-container">
            <i class="fa fa-user pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('student.registration.view')}}"><i class="ti-more"></i>Student Registration</a></li>
          
      

          
        </ul>
 
      </li>


      <li class="header nav-small-cap">User Interface</li>

      <li class="treeview">
        <a href="#">
          <i data-feather="grid"></i>
          <span>Components</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-right pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="components_alerts.html"><i class="ti-more"></i>Alerts</a></li>
          <li><a href="components_badges.html"><i class="ti-more"></i>Badge</a></li>
          <li><a href="components_buttons.html"><i class="ti-more"></i>Buttons</a></li>
      
        </ul>
      </li>
      <li>
        <a href="auth_login.html">
          <i data-feather="lock"></i>
          <span>Lock Screen</span>
        </a>
      </li>

      <li>
        <a href="auth_login.html">
          <i data-feather="lock"></i>
          <span>Log Out</span>
        </a>
      </li>

    </ul>
  </section>

  <div class="sidebar-footer">
    <!-- item-->
    <a href="javascript:void(0)" class="link" data-toggle="tooltip" title="" data-original-title="Settings" aria-describedby="tooltip92529"><i class="ti-settings"></i></a>
    <!-- item-->
    <a href="mailbox_inbox.html" class="link" data-toggle="tooltip" title="" data-original-title="Email"><i class="ti-email"></i></a>
    <!-- item-->
    <a href="{{ route('admin.logout')}}" class="link" data-toggle="tooltip" title="" data-original-title="Logout"><i class="ti-lock"></i></a>
  </div>
</aside>