<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    

    public function ProfileView(Request $request)
    {
        $id = Auth::user()->id;
        $user = User::find($id);
        return  view('backend.user.view_profile',compact('user'));
    }

    public function ProfileEdit(Request $request)
    {
        $id = Auth::user()->id;
        $userData = User::find($id);
        return  view('backend.user.edit_profile',compact('userData'));
    }

    public function ProfileUpdate(Request $request)
    {

        $data = User::find(Auth::user()->id);
        $data->name = $request->name;
        $data->email = $request->email;
        $data->address = $request->address;
        $data->mobile = $request->mobile;
        $data->status = 1;   
        $data->gender = $request->gender;


  if($request->hasFile('image'))
  {
    $file = $request->file('image');
    @unlink(public_path('uploads/user_image/'.$data->image));
    $filename = date('YmdHi').$file->getClientOriginalName();
    $file->move(public_path('uploads/user_image'), $filename);
    $data['image'] = $filename;
  }
  $data->save();
  $notification= array(
    'message' => 'Profile updated sucessfully!',
    'alert-type'=>'success'

);

return redirect()->route('profile.view')->with($notification);

    }


    public function PasswordView()
    {
        return view('backend.user.edit_password');
    }

    public function PasswordUpdate(Request $request)
    {

        $validatedData = $request->validate([
            'current_password'=> 'required',
            'password'=>'required|confirmed'
        ]);

        $hashedPassword =  Auth::user()->password;
        if(Hash::check( $request->current_password,$hashedPassword))
        {
            $user = User::find(Auth::user()->id);
            $user->password = Hash::make($request->password);
            $user->save();
            Auth::logout();

            $notification= array(
                'message' => 'User password updated sucessfully!',
                'alert-type'=>'success'
     
            );
     
            return redirect()->route('login')->with($notification);

        }else{
            return redirect()->back();
        }

 
   

    }
}
