<?php

namespace App\Http\Controllers\Backend\Setup;

use App\Http\Controllers\Controller;
use App\Models\FeeAmount;
use App\Models\FeeCategory;
use App\Models\StudentClass;
use Illuminate\Http\Request;

class FeeAmountController extends Controller
{
    public function ViewFeeAmount(Request $request)
    {
        //$data['allData'] = FeeAmount::all();
        $data['allData'] = FeeAmount::select('fee_category_id')->groupBy('fee_category_id')->get();

        return view('backend.setup.fee_amount.view_amount', $data);

    }

    public function FeeAmountAdd()
    {
        $data['fee_categories']= FeeCategory::all();
        $data['classes'] = StudentClass::all();
        return view('backend.setup.fee_amount.add_amount', $data);

    }
    public function FeeAmountStore(Request $request)
    {

        $countClass = count($request->class_id);
        if(!empty($countClass)){
            for($i = 0; $i < $countClass; $i++){
                $fee_amount = new FeeAmount();
                $fee_amount->fee_category_id = $request->fee_category_id;
                $fee_amount->class_id = $request->class_id[$i];
                $fee_amount->amount = $request->amount[$i];
                $fee_amount->save();
            }

        }

        $notification= array(
            'message' => 'Fee amount saved sucessfully!',
            'alert-type'=>'success'
 
        );
 
        return redirect()->route('fee.amount.view')->with($notification);

    }

    public function FeeAmountEdit(Request $request, $id)
    {
           $data['editData'] = FeeAmount::where('fee_category_id', $id)->orderBy('class_id','asc')->get();
           $data['fee_categories']= FeeCategory::all();
            $data['classes'] = StudentClass::all();
        return view('backend.setup.fee_amount.edit_amount', $data);
    }

    public function FeeAmountUpdate(Request $request, $id)
    {
        if (empty($request->class_id)) {
            $notification= array(
                'message' => 'Please select a class and amount!',
                'alert-type'=>'error'
     
            );
     
            return redirect()->route('fee.amount.edit',$id)->with($notification);
    
            
        } else {

            $countClass = count($request->class_id);
            FeeAmount::where('fee_category_id',$id)->delete();
            for ($i=0; $i < $countClass ; $i++) { 
                $fee_amount = new FeeAmount();
                $fee_amount->fee_category_id = $request->fee_category_id;
                $fee_amount->class_id = $request->class_id[$i];
                $fee_amount->amount = $request->amount[$i];
                $fee_amount->save();
            }
            $notification= array(
                'message' => 'Fee amount updated successfully!',
                'alert-type'=>'success'
     
            );
     
            return redirect()->route('fee.amount.view')->with($notification);
           
        }
        

    }

    public function FeeAmountDetails(Request $request, $id)
    {
        $data['detailsData'] = FeeAmount::where('fee_category_id', $id)->orderBy('class_id','asc')->get();
        return view('backend.setup.fee_amount.details_amount', $data);
       

    }

    public function FeeAmountDestroy(Request $request, $id)
    {
        FeeAmount::where('fee_category_id',$id)->delete();

        $notification= array(
            'message' => 'Fee amount deleted successfully!',
            'alert-type'=>'success'
 
        );
 
        return redirect()->route('fee.amount.view')->with($notification);

    }
}
