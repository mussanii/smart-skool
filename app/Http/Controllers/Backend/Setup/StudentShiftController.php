<?php

namespace App\Http\Controllers\Backend\Setup;

use App\Http\Controllers\Controller;
use App\Models\StudentShift;
use Illuminate\Http\Request;

class StudentShiftController extends Controller
{

    public function ViewStudentshift(Request $request)
    {
        $data['allData']= StudentShift::all();
        return view("backend.setup.student_shift.view_shift", $data);
    }

    public function StudentShiftAdd(Request $request)
    {

        return view('backend.setup.student_shift.add_shift');

    }

    public function StudentShiftStore(Request $request)
    {

        $validateData = $request->validate([
            'name'=>'required|unique:student_groups,name'
        ]);
        $data = new StudentShift();
        $data->name = $request->name;
        $data->save();
        $notification= array(
            'message' => 'Student shift created sucessfully!',
            'alert-type'=>'success'
 
        );
 
        return redirect()->route('student.shift.view')->with($notification);

    }


    public function StudentShiftEdit(Request $request, $id)
    {
        $studentShift = StudentShift::find($id);
        return view('backend.setup.student_shift.edit_shift', compact('studentShift'));

    }

    public function StudentShiftUpdate(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name'=> 'required',
            
           
        ]);

        $studentShift = StudentShift::find($id);
        $studentShift->name = $request->name;
        $studentShift->save();

        $notification= array(
            'message' => 'Student shift updated sucessfully!',
            'alert-type'=>'info'
 
        );
 
        return redirect()->route('student.shift.view')->with($notification);

    }


    public function StudentShiftDestroy($id)
    {
        $group = StudentShift::find($id);
        $group->delete();

        $notification= array(
            'message' => 'Student shift deleted sucessfully!',
            'alert-type'=>'success'
 
        );
 
        return redirect()->route('student.shift.view')->with($notification);
    }
    //
}
