<?php

namespace App\Http\Controllers\Backend\Setup;

use App\Http\Controllers\Controller;
use App\Models\SchoolSubject;
use Illuminate\Http\Request;

class SchoolSubjectController extends Controller
{
    public function ViewSchoolSubject()
    {
        $data['allData']= SchoolSubject::all();
         return view('backend.setup.subject.view_subject', $data);

    }

    public function SchoolSubjectAdd(Request $request)
    {

        return view('backend.setup.subject.add_subject');

    }

    public function SchoolSubjectStore(Request $request)
    {

        $validateData = $request->validate([
            'name'=>'required|unique:school_subjects,name'
        ]);
        $data = new SchoolSubject();
        $data->name = $request->name;
        $data->save();
        $notification= array(
            'message' => 'Subject created sucessfully!',
            'alert-type'=>'success'
 
        );
 
        return redirect()->route('school.subject.view')->with($notification);

    }

    public function SchoolSubjectEdit(Request $request, $id)
    {
        $schoolSubject = SchoolSubject::find($id);
        return view('backend.setup.subject.edit_subject', compact('schoolSubject'));

    }

    public function SchoolSubjectUpdate(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name'=> 'required',
            
           
        ]);

        $schoolSubject = SchoolSubject::find($id);
        $schoolSubject->name = $request->name;
        $schoolSubject->save();

        $notification= array(
            'message' => 'Subject updated sucessfully!',
            'alert-type'=>'info'
 
        );
 
        return redirect()->route('school.subject.view')->with($notification);

    }


    public function SchoolSubjectDestroy($id)
    {
        $subject = SchoolSubject::find($id);
        $subject->delete();

        $notification= array(
            'message' => 'Subject deleted sucessfully!',
            'alert-type'=>'success'
 
        );
 
        return redirect()->route('school.subject.view')->with($notification);
    }
    
    
}
