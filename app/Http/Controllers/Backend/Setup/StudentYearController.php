<?php

namespace App\Http\Controllers\Backend\Setup;

use App\Http\Controllers\Controller;
use App\Models\StudentYear;
use Illuminate\Http\Request;

class StudentYearController extends Controller
{
    public function ViewStudentYear(Request $request)
    {
        $data['allData']= StudentYear::all();
        return view("backend.setup.student_year.view_year", $data);
    }

    public function StudentYearAdd(Request $request)
    {

        return view('backend.setup.student_year.add_year');

    }

    public function StudentYearStore(Request $request)
    {

        $validateData = $request->validate([
            'name'=>'required|unique:student_years,name'
        ]);
        $data = new StudentYear();
        $data->name = $request->name;
        $data->save();
        $notification= array(
            'message' => 'Student year created sucessfully!',
            'alert-type'=>'success'
 
        );
 
        return redirect()->route('student.year.view')->with($notification);

    }


    public function StudentYearEdit(Request $request, $id)
    {
        $studentYear = StudentYear::find($id);
        return view('backend.setup.student_year.edit_year', compact('studentYear'));

    }

    public function StudentYearUpdate(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name'=> 'required',
            
           
        ]);

        $studentYear = StudentYear::find($id);
        $studentYear->name = $request->name;
        $studentYear->save();

        $notification= array(
            'message' => 'Student year updated sucessfully!',
            'alert-type'=>'info'
 
        );
 
        return redirect()->route('student.year.view')->with($notification);

    }


    public function StudentYearDestroy($id)
    {
        $year = StudentYear::find($id);
        $year->delete();

        $notification= array(
            'message' => 'Student year deleted sucessfully!',
            'alert-type'=>'success'
 
        );
 
        return redirect()->route('student.year.view')->with($notification);
    }
}
