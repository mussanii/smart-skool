<?php

namespace App\Http\Controllers\Backend\Setup;

use App\Http\Controllers\Controller;
use App\Models\StudentClass;
use Illuminate\Http\Request;

class StudentClassController extends Controller
{
    
    public function ViewStudent()
    {
        $data['allData']= StudentClass::all();
         return view('backend.setup.student_class.view_class', $data);

    }

    public function StudentClassAdd(Request $request)
    {

        return view('backend.setup.student_class.add_class');

    }

    public function StudentClassStore(Request $request)
    {

        $validateData = $request->validate([
            'name'=>'required|unique:student_classes,name'
        ]);
        $data = new StudentClass();
        $data->name = $request->name;
        $data->save();
        $notification= array(
            'message' => 'Student class created sucessfully!',
            'alert-type'=>'success'
 
        );
 
        return redirect()->route('student.class.view')->with($notification);

    }

    public function StudentClassEdit(Request $request, $id)
    {
        $studentClass = StudentClass::find($id);
        return view('backend.setup.student_class.edit_class', compact('studentClass'));

    }

    public function StudentClassUpdate(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name'=> 'required',
            
           
        ]);

        $studentClass = StudentClass::find($id);
        $studentClass->name = $request->name;
        $studentClass->save();

        $notification= array(
            'message' => 'Student class updated sucessfully!',
            'alert-type'=>'info'
 
        );
 
        return redirect()->route('student.class.view')->with($notification);

    }


    public function StudentClassDestroy($id)
    {
        $class = StudentClass::find($id);
        $class->delete();

        $notification= array(
            'message' => 'Student class deleted sucessfully!',
            'alert-type'=>'success'
 
        );
 
        return redirect()->route('student.class.view')->with($notification);
    }
}
