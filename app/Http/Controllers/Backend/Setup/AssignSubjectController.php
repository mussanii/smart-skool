<?php

namespace App\Http\Controllers\Backend\Setup;

use App\Http\Controllers\Controller;
use App\Models\AssignSubject;
use App\Models\SchoolSubject;
use App\Models\StudentClass;
use Illuminate\Http\Request;

class AssignSubjectController extends Controller
{
    public function ViewAssignSubject(Request $request)
    {
        //$data['allData'] = AssignSubject::all();
        $data['allData'] = AssignSubject::select('class_id')->groupBy('class_id')->get();

        return view('backend.setup.assign_subject.view_assign_subject',$data);

    }

    public function AssignSubjectAdd()
    {
        $data['subjects'] = SchoolSubject::all();
        $data['classes'] = StudentClass::all();
        return view('backend.setup.assign_subject.add_assign_subject', $data);

    }
    public function AssignSubjectStore(Request $request)
    {

       

        $countSubject = count($request->subject_id);
      ;
        if(!empty($countSubject)){
            for($i = 0; $i < $countSubject; $i++){
                $assign_subject = new AssignSubject();
                $assign_subject->class_id = $request->class_id;
                $assign_subject->subject_id = $request->subject_id[$i];
                $assign_subject->full_mark = $request->full_mark[$i];
                $assign_subject->pass_mark = $request->pass_mark[$i];
                $assign_subject->subjective_mark = $request->subjective_mark[$i];
                $assign_subject->save();
            }

        }

        $notification= array(
            'message' => 'Subject assignment saved sucessfully!',
            'alert-type'=>'success'
 
        );
 
        return redirect()->route('assign.subject.view')->with($notification);

    }

    public function AssignSubjectEdit(Request $request, $id)
    {
           $data['editData'] = AssignSubject::where('class_id', $id)->orderBy('subject_id','asc')->get();
           $data['subjects']= SchoolSubject::all();
            $data['classes'] = StudentClass::all();
        return view('backend.setup.assign_subject.edit_assign_subject', $data);
    }

    public function AssignSubjectUpdate(Request $request, $id)
    {
        if (empty($request->class_id)) {
            $notification= array(
                'message' => 'Please select a subject and enter the marks!',
                'alert-type'=>'error'
     
            );
     
            return redirect()->route('assign.subject.edit',$id)->with($notification);
    
            
        } else {

            $countSubject = count($request->subject_id);
            AssignSubject::where('class_id',$id)->delete();
            for ($i=0; $i < $countSubject ; $i++) { 
                $assign_subject = new AssignSubject();

                $assign_subject->class_id = $request->class_id;
                $assign_subject->subject_id = $request->subject_id[$i];
                $assign_subject->full_mark = $request->full_mark[$i];
                $assign_subject->pass_mark = $request->pass_mark[$i];
                $assign_subject->subjective_mark = $request->subjective_mark[$i];
                $assign_subject->save();
            }
            $notification= array(
                'message' => 'Assign Subject updated successfully!',
                'alert-type'=>'success'
     
            );
     
            return redirect()->route('assign.subject.view')->with($notification);
           
        }
        

    }

    public function AssignSubjectDetails(Request $request, $id)
    {
        $data['detailsData'] = AssignSubject::where('class_id', $id)->orderBy('subject_id','asc')->get();
        return view('backend.setup.assign_subject.details_assign_subject', $data);
       

    }

    public function AssignSubjectDestroy(Request $request, $id)
    {
        AssignSubject::where('class_id',$id)->delete();

        $notification= array(
            'message' => 'Assign Subjects deleted successfully!',
            'alert-type'=>'success'
 
        );
 
        return redirect()->route('assign.subject.view')->with($notification);

    }
}
