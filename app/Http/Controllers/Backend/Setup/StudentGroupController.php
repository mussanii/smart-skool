<?php

namespace App\Http\Controllers\Backend\Setup;

use App\Http\Controllers\Controller;
use App\Models\StudentGroup;
use Illuminate\Http\Request;

class StudentGroupController extends Controller
{

    public function ViewStudentGroup(Request $request)
    {
        $data['allData']= StudentGroup::all();
        return view("backend.setup.student_group.view_group", $data);
    }

    public function StudentGroupAdd(Request $request)
    {

        return view('backend.setup.student_group.add_group');

    }

    public function StudentGroupStore(Request $request)
    {

        $validateData = $request->validate([
            'name'=>'required|unique:student_groups,name'
        ]);
        $data = new StudentGroup();
        $data->name = $request->name;
        $data->save();
        $notification= array(
            'message' => 'Student group created sucessfully!',
            'alert-type'=>'success'
 
        );
 
        return redirect()->route('student.group.view')->with($notification);

    }


    public function StudentGroupEdit(Request $request, $id)
    {
        $studentGroup = StudentGroup::find($id);
        return view('backend.setup.student_group.edit_group', compact('studentGroup'));

    }

    public function StudentGroupUpdate(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name'=> 'required',
            
           
        ]);

        $studentGroup = StudentGroup::find($id);
        $studentGroup->name = $request->name;
        $studentGroup->save();

        $notification= array(
            'message' => 'Student group updated sucessfully!',
            'alert-type'=>'info'
 
        );
 
        return redirect()->route('student.group.view')->with($notification);

    }


    public function StudentGroupDestroy($id)
    {
        $group = StudentGroup::find($id);
        $group->delete();

        $notification= array(
            'message' => 'Student group deleted sucessfully!',
            'alert-type'=>'success'
 
        );
 
        return redirect()->route('student.group.view')->with($notification);
    }
    //
}
