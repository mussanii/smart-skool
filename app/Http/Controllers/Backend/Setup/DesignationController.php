<?php

namespace App\Http\Controllers\Backend\Setup;

use App\Http\Controllers\Controller;
use App\Models\Designation;
use Illuminate\Http\Request;

class DesignationController extends Controller
{
    public function ViewDesignation(Request $request)
    {
        $data['allData']= Designation::all();
        return view("backend.setup.designation.view_designation", $data);
    }

    public function DesignationAdd(Request $request)
    {

        return view("backend.setup.designation.add_designation");

    }

    public function DesignationStore(Request $request)
    {

        

        $validateData = $request->validate([
            'name'=>'required|unique:designations,name'
        ]);
        $data = new Designation();
        $data->name = $request->name;
        $data->save();
        $notification= array(
            'message' => 'Designation created sucessfully!',
            'alert-type'=>'success'
 
        );
 
        return redirect()->route('designation.view')->with($notification);

    }


    public function DesignationEdit(Request $request, $id)
    {
        $designation = Designation::find($id);
        return view('backend.setup.designation.edit_designation', compact('designation'));

    }

    public function DesignationUpdate(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name'=> 'required',
            
           
        ]);

        $examType = Designation::find($id);
        $examType->name = $request->name;
        $examType->save();

        $notification= array(
            'message' => 'Designation updated sucessfully!',
            'alert-type'=>'info'
 
        );
 
        return redirect()->route('designation.view')->with($notification);

    }


    public function DesignationDestroy($id)
    {
        $designation = Designation::find($id);
        $designation->delete();

        $notification= array(
            'message' => 'Designation deleted sucessfully!',
            'alert-type'=>'success'
 
        );
 
        return redirect()->route('designation.view')->with($notification);
    }
}
