<?php

namespace App\Http\Controllers\Backend\Setup;

use App\Http\Controllers\Controller;
use App\Models\FeeCategory;
use Illuminate\Http\Request;

class FeeCategoryController extends Controller
{
    
    public function ViewFeeCategory()
    {
        $data['allData']= FeeCategory::all();
         return view('backend.setup.fee_category.view_fee', $data);

    }

    public function FeeCategoryAdd(Request $request)
    {

        return view('backend.setup.fee_category.add_fee');

    }

    public function FeeCategoryStore(Request $request)
    {

        $validateData = $request->validate([
            'name'=>'required|unique:fee_categories,name'
        ]);
        $data = new FeeCategory();
        $data->name = $request->name;
        $data->save();
        $notification= array(
            'message' => 'Fee category created sucessfully!',
            'alert-type'=>'success'
 
        );
 
        return redirect()->route('fee.category.view')->with($notification);

    }

    public function FeeCategoryEdit(Request $request, $id)
    {
        $studentFee = FeeCategory::find($id);
        return view('backend.setup.fee_category.edit_fee', compact('studentFee'));

    }

    public function FeeCategoryUpdate(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name'=> 'required',
            
           
        ]);

        $studentFee = FeeCategory::find($id);
        $studentFee->name = $request->name;
        $studentFee->save();

        $notification= array(
            'message' => 'Fee category updated sucessfully!',
            'alert-type'=>'info'
 
        );
 
        return redirect()->route('fee.category.view')->with($notification);

    }


    public function FeeCategoryDestroy($id)
    {
        $class = FeeCategory::find($id);
        $class->delete();

        $notification= array(
            'message' => 'Fee category deleted sucessfully!',
            'alert-type'=>'success'
 
        );
 
        return redirect()->route('fee.category.view')->with($notification);
    }
}
