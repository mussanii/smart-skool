<?php

namespace App\Http\Controllers\Backend\Setup;

use App\Http\Controllers\Controller;
use App\Models\ExamType;
use Illuminate\Http\Request;

class ExamTypeController extends Controller
{
    public function ViewExamType(Request $request)
    {
        $data['allData']= ExamType::all();
        return view("backend.setup.exam_type.view_exam_type", $data);
    }

    public function ExamTypeAdd(Request $request)
    {

        return view("backend.setup.exam_type.add_exam_type");

    }

    public function ExamTypeStore(Request $request)
    {

        

        $validateData = $request->validate([
            'name'=>'required|unique:exam_types,name'
        ]);
        $data = new ExamType();
        $data->name = $request->name;
        $data->save();
        $notification= array(
            'message' => 'Exam type created sucessfully!',
            'alert-type'=>'success'
 
        );
 
        return redirect()->route('exam.type.view')->with($notification);

    }


    public function ExamTypeEdit(Request $request, $id)
    {
        $examType = ExamType::find($id);
        return view('backend.setup.exam_type.edit_exam_type', compact('examType'));

    }

    public function ExamTypeUpdate(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name'=> 'required',
            
           
        ]);

        $examType = ExamType::find($id);
        $examType->name = $request->name;
        $examType->save();

        $notification= array(
            'message' => 'Exam type updated sucessfully!',
            'alert-type'=>'info'
 
        );
 
        return redirect()->route('exam.type.view')->with($notification);

    }


    public function ExamTypeDestroy($id)
    {
        $year = ExamType::find($id);
        $year->delete();

        $notification= array(
            'message' => 'Exam type deleted sucessfully!',
            'alert-type'=>'success'
 
        );
 
        return redirect()->route('exam.type.view')->with($notification);
    }
}
