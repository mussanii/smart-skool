<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function UserView()
    {
        $users = User::all();
        //$data['allData'] = User::all();
        return view('backend.user.view_user', compact('users'));
    }

    public function UserAdd()
    {
        return view('backend.user.add_user');
    }



    public function userStore(Request $request)
    {
       $validatedData = $request->validate([
           'email'=> 'required|unique:users',
           'name'=>'required'
       ]);

       $user = new User();
       $user->usertype = $request->usertype;
       $user->name  = $request->name;
       $user->email = $request->email;
       $user->password = BCRYPT($request->password);

       $user->save();

       $notification= array(
           'message' => 'User created sucessfully!',
           'alert-type'=>'success'

       );

       return redirect()->route('user.view')->with($notification);
    }


    public function UserEdit($id) {
        $userData = User::find($id);
        return view('backend.user.edit_user', compact('userData'));
    }

    public function UserUpdate(Request $request, $id)
    {

        
        $validatedData = $request->validate([
            'email'=> 'required|unique:users',
            'name'=>'required',
           
        ]);

     
 
        $user = User::find($id);
       

       
        $user->usertype = $request->usertype;
        $user->status = $request->status;
        $user->name  = $request->name;
        $user->email = $request->email;
        $user->save();
 
        $notification= array(
            'message' => 'User updated sucessfully!',
            'alert-type'=>'info'
 
        );
 
        return redirect()->route('user.view')->with($notification);
    }

    public function UserDestroy($id)
    {
        $user = User::find($id);
        $user->delete();

        $notification= array(
            'message' => 'User deleted sucessfully!',
            'alert-type'=>'success'
 
        );
 
        return redirect()->route('user.view')->with($notification);
    }
}
